import React, { useEffect, useState } from "react";
import { Dropdown,
         DropdownItem,
         DropdownToggle,
         Page,
         PageSection,
         PageHeader,
         PageHeaderTools,
         PageHeaderToolsItem,
         Nav,
         NavItem,
         NavList } from '@patternfly/react-core';
import Link from 'next/link'
import axios from 'axios'
import { useRouter } from 'next/router'
import package_info from '../package.json'

function MainLayout({children}) {

  const router = useRouter()

  const HeaderTools = (
    <PageHeaderTools>
      <PageHeaderToolsItem>
        <Nav variant="horizontal">
          <NavList>
            <NavItem key="auth" itemId={1} isActive={router.pathname?.startsWith('/authorities')}>
              <Link href="/authorities"><a className="pf-c-nav__link" href="/authorities">Authorities</a></Link>
            </NavItem>
            <NavItem key="mult" itemId={2} isActive={false}>
              <Link href="/multiplexes"><a className="pf-c-nav__link" href="/multiplexes">Multiplexes</a></Link>
            </NavItem>
            <NavItem key="serv" itemId={3} isActive={false}>
              <Link href="/services"><a className="pf-c-nav__link" href="/services">Services</a></Link>
            </NavItem>
            <NavItem key="tran" itemId={4} isActive={false}>
              <Link href="/transmitters"><a className="pf-c-nav__link" href="/transmitters">Transmitters</a></Link>
            </NavItem>
          </NavList>
        </Nav>
      </PageHeaderToolsItem>
    </PageHeaderTools>
  );

  const Header = (
    <PageHeader showNavToggle
                logo={`MQTTDemo ${package_info.version}`}
                headerTools={HeaderTools}
                />
  );

  return (
    <Page mainContainerId="holoActiveComponent" header={Header} >
      <PageSection isFilled={true} hasOverflowScroll={true}>
        {children}
      </PageSection>
      <PageSection>
        <div style={{"textAlign": "right"}}>
          MQTTDemo {package_info.version} ({process.env.NEXT_PUBLIC_ENV_LABEL})
        </div>
      </PageSection>
    </Page>
  )
}

export default MainLayout;

